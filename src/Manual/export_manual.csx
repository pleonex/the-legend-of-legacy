#!/usr/bin/env dotnet-script

#r "../../GameData/AttackFridayMonsters.Formats.dll"
#r "nuget: Yarhl, 4.0.0-preview.159"
#r "nuget: Yarhl.Media, 4.0.0-preview.159"
#r "nuget: YamlDotNet, 8.0.0"

using System.IO;
using AttackFridayMonsters.Formats.Container;
using AttackFridayMonsters.Formats.Compression;
using AttackFridayMonsters.Formats.Text.Code;
using AttackFridayMonsters.Formats.Text.Layout;
using AttackFridayMonsters.Formats.Text;
using Yarhl.FileSystem;
using Yarhl.FileFormat;
using Yarhl.Media.Text;
using Yarhl.IO;
using AttackFridayMonsters.Formats;


void ExportManual()
{
    Node manual = NodeFactory.FromFile("GameData/Manual.bcma")
        .TransformWith<DarcToBinary>();

    Node manualSpanish = manual.Children["."].Children["EUR_es_small.arc"]
        .TransformWith<NitroLz10Decompression>()
        .TransformWith<DarcToBinary>();

    Node page = manualSpanish.Children["."].Children["blyt"].Children["Page_000_small_0.bclyt"]
        .TransformWith<Binary2Clyt>();

    ((BinaryFormat)ConvertFormat.With<Clyt2Yml>(page))
        .Stream.WriteTo("GameData/page0.yml");

    Po po = (Po)ConvertFormat.With<Clyt2Po>(page);
    ((BinaryFormat)ConvertFormat.With<Po2Binary>(po))
        .Stream.WriteTo("GameData/page0.po");
}

public class NitroLz10Decompression : IConverter<IBinary, BinaryFormat>
{
    private const byte Stamp = 0x10;
    private const byte MinSequenceLength = 2;

    /// <summary>
    /// Decompress a LZSS-compressed stream.
    /// </summary>
    /// <param name="source">The stream to convert.</param>
    /// <returns>The decompressed in-memory stream.</returns>
    public BinaryFormat Convert(IBinary source)
    {
        if (source is null)
            throw new ArgumentNullException(nameof(source));

        source.Stream.Position = 0;
        var reader = new DataReader(source.Stream);

        int outputLen = ReadHeader(reader);

        // Read and write into arrays as it's faster
        int inputLen = (int)(source.Stream.Length - 4);
        int inputPos = 0;
        byte[] input = reader.ReadBytes(inputLen);
        int outputPos = 0;
        byte[] output = new byte[outputLen];

        byte mask = 0;
        byte flags = 0;
        while (inputPos < inputLen) {
            if (mask == 0) {
                flags = input[inputPos++];
                mask = 0x80;
            }

            bool isRaw = (flags & mask) == 0;
            mask >>= 1;

            if (isRaw) {
                output[outputPos++] = input[inputPos++];
            } else {
                DecodeCopy(input, ref inputPos, output, ref outputPos);
            }
        }

        return new BinaryFormat(DataStreamFactory.FromArray(output, 0, output.Length));
    }

    private static int ReadHeader(DataReader reader)
    {
        uint header = reader.ReadUInt32();
        uint id = header & 0xFF;
        uint outputLen = header >> 8; // max 16 MB
        if (id != Stamp) {
            throw new FormatException("Invalid compression ID");
        }

        return (int)outputLen;
    }

    private static void DecodeCopy(ReadOnlySpan<byte> input, ref int inputPos, Span<byte> output, ref int outputPos)
    {
        // bits 0-11: back position
        // bits 12-X: length
        int flags = input[inputPos++];
        flags = (flags << 8) | input[inputPos++];

        int minLength = 3;
        int len = (flags >> 12) + minLength;
        int pos = (flags & 0xFFF) + 1;
        for (int i = 0; i < len; i++) {
            output[outputPos] = output[outputPos - pos];
            outputPos++;
        }
    }
}
