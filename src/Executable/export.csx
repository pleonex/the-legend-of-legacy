#!/usr/bin/env dotnet-script

#r "nuget: Yarhl, 4.0.0-preview.159"
#r "nuget: Yarhl.Media, 4.0.0-preview.159"
#r "nuget: YamlDotNet, 12.0.0"
#load "ExecutableConverters.csx"

using System.IO;
using Yarhl.FileSystem;
using Yarhl.FileFormat;
using Yarhl.Media.Text;
using Yarhl.IO;

if (Args.Count != 3) {
    Console.WriteLine("USAGE: CodeExporter <StringDefinition.yaml> <DecompressedCode.bin> <outputPoPath>");
    return;
}

ExportProgramText(Args[0], Args[1], Args[2]);

void ExportProgramText(string yamlPath, string codePath, string outputPath)
{
    Console.WriteLine("Extracting text from code.bin");
    DataStream stringDefinitions = DataStreamFactory.FromFile(
        yamlPath,
        FileOpenMode.Read);

    // Decompress binary
    // var compressionConverter = new ExternalProgramConverter {
    //    Program = "GameData/blz",
    //    Arguments = "-d <inout>",
    // };

    NodeFactory.FromFile(codePath)
        // .TransformWith(compressionConverter)
        .TransformWith<BinaryStrings2Po, DataStream>(stringDefinitions)
        .TransformWith<Po2Binary>()
        .Stream.WriteTo(outputPath);
}
