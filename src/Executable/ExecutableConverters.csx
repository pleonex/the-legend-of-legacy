#r "nuget: Yarhl, 4.0.0-preview.159"
#r "nuget: Yarhl.Media, 4.0.0-preview.159"
#r "nuget: YamlDotNet, 12.0.0"

using System.Collections.ObjectModel;
using System.Globalization;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using Yarhl.FileFormat;
using Yarhl.IO;
using Yarhl.Media.Text;

public class ExternalProgramConverter : IConverter<BinaryFormat, BinaryFormat>
{
    public string Program { get; set; }

    public string Arguments { get; set; }

    public string WorkingDirectory { get; set; }

    public string FileName { get; set; }

    public BinaryFormat Convert(BinaryFormat source)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));

        if (!Arguments.Contains("<in>") && !Arguments.Contains("<inout>"))
            throw new FormatException("Missing input in arguments");

        // Save stream into temporal file
        string tempInputFolder = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        Directory.CreateDirectory(tempInputFolder);

        string tempInputName = string.IsNullOrEmpty(FileName) ? "input.bin" : FileName;
        string tempInputFile = Path.Combine(tempInputFolder, tempInputName);
        source.Stream.WriteTo(tempInputFile);

        // Get or create the output file
        string tempOutputFile;
        if (Arguments.Contains("<out>")) {
            tempOutputFile = Path.GetTempFileName();
        } else if (Arguments.Contains("<inout>")) {
            tempOutputFile = tempInputFile;
        } else {
            throw new FormatException("Missing output in arguments");
        }

        // Run the process
        string args = Arguments.Replace("<in>", tempInputFile)
            .Replace("<inout>", tempInputFile)
            .Replace("<out>", tempOutputFile);

        var process = new Process();
        process.StartInfo.FileName = Program;
        process.StartInfo.Arguments = args;
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.CreateNoWindow = true;
        process.StartInfo.ErrorDialog = false;
        process.StartInfo.RedirectStandardOutput = true;
        process.StartInfo.RedirectStandardError = true;

        if (!string.IsNullOrEmpty(WorkingDirectory)) {
            if (!Directory.Exists(WorkingDirectory)) {
                Directory.CreateDirectory(WorkingDirectory);
            }

            process.StartInfo.WorkingDirectory = WorkingDirectory;
        }

        process.Start();
        process.WaitForExit();

        if (process.ExitCode != 0) {
            Directory.Delete(tempInputFolder, true);
            DeleteIfExists(tempOutputFile);
            throw new Exception($"Error running: {Program} {args} {process.StandardError.ReadToEnd()} {process.StandardOutput.ReadToEnd()}");
        }

        // Read the file into memory so we can delete it.
        var convertedStream = new BinaryFormat();
        using (var tempStream = DataStreamFactory.FromFile(tempOutputFile, FileOpenMode.Read)) {
            tempStream.WriteTo(convertedStream.Stream);
        }

        Directory.Delete(tempInputFolder, true);
        DeleteIfExists(tempOutputFile);
        return convertedStream;
    }

    static void DeleteIfExists(string path)
    {
        if (File.Exists(path)) {
            File.Delete(path);
        }
    }
}

public sealed class BinaryStrings2Po : IConverter<BinaryFormat, Po>, IInitializer<DataStream>
{
    StringDefinitionBlock block;

    public void Initialize(DataStream parameters)
    {
        if (parameters == null)
            throw new ArgumentNullException(nameof(parameters));

        string yaml = new TextDataReader(parameters).ReadToEnd();
        block = new DeserializerBuilder()
            .WithNamingConvention(CamelCaseNamingConvention.Instance)
            .Build()
            .Deserialize<StringDefinitionBlock>(yaml);
    }

    public Po Convert(BinaryFormat source)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));
        if (block == null)
            throw new InvalidOperationException("Converter not initialized");

        Po po = new Po {
            Header = new PoHeader(
                "The Legends of Legacy",
                "tradusquare@gmail.com",
                "es-ES"),
        };

        DataReader reader = new DataReader(source.Stream);
        foreach (var definition in block.Definitions) {
            source.Stream.Position = definition.Address - block.Offset[0].Ram;
            var encoding = Encoding.GetEncoding(definition.Encoding);
            string text = reader.ReadString(definition.Size, encoding).Replace("\0", string.Empty);
            text = text.Replace("\u0001", "{color1}")
                .Replace("\u0002", "{color2}")
                .Replace("\u0003", "{color3}")
                .Replace("\u0004", "{color4}");

            string pointers = string.Join(",", definition.Pointers?.Select(p => $"0x{p:X}") ?? Enumerable.Empty<string>());

            var entry = new PoEntry {
                Original = text,
                Context = $"0x{definition.Address:X8}",
                Flags = "c-format,brace-format",
                Reference = $"0x{definition.Address:X}:{definition.Size}:{definition.Encoding}:{pointers}",
            };
            po.Add(entry);
        }

        return po;
    }
}

public class Code3dsPoImporter :
    IInitializer<(Po, DataStream)>, IConverter<BinaryFormat, BinaryFormat>
{
    Po texts;
    DataStream exHeader;
    ExtendedHeaderObjInfo codeInfo;

    long ramOffset;
    DataWriter writer;

    public void Initialize((Po, DataStream) parameters)
    {
        texts = parameters.Item1;
        exHeader = parameters.Item2;
        codeInfo = ReadExtendedHeader(exHeader);
        ramOffset = codeInfo.TextSection.RamAddress;
    }

    public BinaryFormat Convert(BinaryFormat source)
    {
        // Do not overwrite same file, create a copy
        var updatedCode = new BinaryFormat();
        source.Stream.WriteTo(updatedCode.Stream);

        writer = new DataWriter(updatedCode.Stream);

        bool updateExHeader = false;
        foreach (var entry in texts.Entries) {
            StringDefinition definition;
            try {
                definition = GetDefinition(entry.Reference);
            } catch (Exception) {
                Console.WriteLine($"Cannot parse '{entry.Reference}'");
                throw;
            }

            byte[] text = EncodeText(entry.Text, definition.Encoding);

            if (TryImportInPlace(text, definition)) {
                continue;
            }

            updateExHeader = true;
            if (!TryImportInPadding(text, definition)) {
                throw new FormatException($"The text doesn't fit in file {entry.Text}");
            }
        }

        if (updateExHeader) {
            WriteExtendedHeader(exHeader, codeInfo);
        }

        return updatedCode;
    }

    static ExtendedHeaderObjInfo ReadExtendedHeader(DataStream stream)
    {
        var reader = new DataReader(stream);
        var info = new ExtendedHeaderObjInfo();

        stream.Position = 0x10;
        info.TextSection = new SectionInfo {
            RamAddress = reader.ReadUInt32(),
            PhysicalSize = reader.ReadUInt32() << 12,
            Size = reader.ReadUInt32(),
        };

        stream.Position = 0x20;
        info.ReadOnlySection = new SectionInfo {
            RamAddress = reader.ReadUInt32(),
            PhysicalSize = reader.ReadUInt32() << 12,
            Size = reader.ReadUInt32(),
        };

        stream.Position = 0x30;
        info.DataSection = new SectionInfo {
            RamAddress = reader.ReadUInt32(),
            PhysicalSize = reader.ReadUInt32() << 12,
            Size = reader.ReadUInt32(),
        };


        return info;
    }

    static void WriteExtendedHeader(DataStream stream, ExtendedHeaderObjInfo info)
    {
        var writer = new DataWriter(stream);

        stream.Position = 0x10;
        writer.Write((uint)info.TextSection.RamAddress);
        writer.Write((uint)(info.TextSection.PhysicalSize >> 12));
        writer.Write((uint)info.TextSection.Size);

        stream.Position = 0x20;
        writer.Write((uint)info.ReadOnlySection.RamAddress);
        writer.Write((uint)(info.ReadOnlySection.PhysicalSize >> 12));
        writer.Write((uint)info.ReadOnlySection.Size);

        stream.Position = 0x30;
        writer.Write((uint)info.DataSection.RamAddress);
        writer.Write((uint)(info.DataSection.PhysicalSize >> 12));
        writer.Write((uint)info.DataSection.Size);
    }

    static StringDefinition GetDefinition(string reference)
    {
        string[] segments = reference.Split(':');
        if (segments.Length != 4 && segments.Length != 5) {
            throw new FormatException($"Invalid number of segments: {reference}");
        }

        var pointers = segments[3].Split(',', StringSplitOptions.RemoveEmptyEntries)
            .Select(x => long.Parse(x.Substring(2), NumberStyles.HexNumber));

        var subpointers = Enumerable.Empty<Subpointer>();
        if (segments.Length == 5) {
            subpointers = segments[4].Split(',')
                .Select(x => x.Split('_'))
                .Select(x => (long.Parse(x[0].Substring(2), NumberStyles.HexNumber), x[1]))
                .Select(x => (x.Item1, int.Parse(x.Item2)))
                .Select(x => new Subpointer { Address = x.Item1, Offset = x.Item2 });
        }

        return new StringDefinition {
            Address = int.Parse(segments[0].Substring(2), NumberStyles.HexNumber),
            Size = int.Parse(segments[1]),
            Encoding = segments[2],
            Pointers = new Collection<long>(pointers.ToList()),
            Subpointers = new Collection<Subpointer>(subpointers.ToList()),
        };
    }

    static byte[] EncodeText(string text, string encodingName)
    {
        text = text.Replace("{color1}", "\u0001")
                .Replace("{color2}", "\u0002")
                .Replace("{color3}", "\u0003")
                .Replace("{color4}", "\u0004");
        var encoding = Encoding.GetEncoding(encodingName);
        return encoding.GetBytes(text + '\0');
    }

    static SectionInfo FindSection(long address, params SectionInfo[] sections)
    {
        foreach (var section in sections) {
            if (address >= section.RamAddress && address < section.RamAddress + section.Size) {
                return section;
            }
        }

        throw new FormatException($"Invalid address: 0x{address:X8}");
    }

    bool TryImportInPlace(byte[] text, StringDefinition definition)
    {
        if (text.Length > definition.Size) {
            return false;
        }

        writer.Stream.Position = definition.Address - ramOffset;
        writer.Write(text);
        return true;
    }

    bool TryImportInPadding(byte[] text, StringDefinition definition)
    {
        SectionInfo section = FindSection(
            definition.Address,
            codeInfo.TextSection,
            codeInfo.ReadOnlySection,
            codeInfo.DataSection);

        if (section.PaddingSize < text.Length) {
            return false;
        }

        // Put at the end
        long oldAddress = definition.Address;
        definition.Address = section.RamAddress + section.Size;
        writer.Stream.Position = definition.Address - ramOffset;
        writer.Write(text);
        section.Size += text.Length;

        if (definition.Pointers.Count == 0) {
            Console.WriteLine(
                $"WARNING moving oversized text into new position 0x{definition.Address:X8}. " +
                $"The entry 0x{oldAddress:X8} has no pointers. Please review, manual update pointer or reduce length of text.");
        }

        // Update pointers
        var reader = new DataReader(writer.Stream);
        foreach (var pointerAddr in definition.Pointers) {
            // Skip indirect pointers (like accessing from code sums).
            // It may be because it's a reference to another pointer
            // or because it was the result of code (e.g. additions).
            // It may require manual code changes.
            reader.Stream.Position = pointerAddr - ramOffset;
            uint currentPtr = reader.ReadUInt32();
            if (currentPtr != oldAddress) {
                Console.WriteLine(
                    $"WARNING moving oversized text into new position 0x{definition.Address:X8}. " +
                    $"Cannot update pointer 0x{pointerAddr:X8} for entry 0x{oldAddress:X8}. " +
                    "Indirect pointer (opcode or pointer to pointer) found. Please review, manual update pointer or reduce length of text.");
                continue;
            }

            writer.Stream.Position = pointerAddr - ramOffset;
            writer.Write((uint)definition.Address);
        }

        foreach (var subpointer in definition.Subpointers) {
            long relativePointer = definition.Address + subpointer.Offset;

            writer.Stream.Position = subpointer.Address - ramOffset;
            writer.Write((uint)relativePointer);
        }

        return true;
    }

    private sealed class ExtendedHeaderObjInfo
    {
        public SectionInfo TextSection { get; set; }

        public SectionInfo ReadOnlySection { get; set; }

        public SectionInfo DataSection { get; set; }
    }

    private sealed class SectionInfo
    {
        public long RamAddress { get; set; }

        public long PhysicalSize { get; set; }

        public long Size { get; set; }

        public long PaddingSize => PhysicalSize - Size;
    }
}

public class StringDefinitionBlock
{
    public Collection<MemoryBlock> Offset { get; set; }

    public Collection<StringDefinition> Definitions { get; set; }
}

public class MemoryBlock
{
    public string Name { get; set; }

    public long Ram { get; set; }

    public long File { get; set; }
}

public class StringDefinition
{
    public long Address { get; set; }

    public int Size { get; set; }

    public string Encoding { get; set; }

    public Collection<long> Pointers { get; set; }

    public Collection<Subpointer> Subpointers { get; set; }
}

public class Subpointer
{
    public long Address { get; set; }

    public int Offset { get; set; }
}