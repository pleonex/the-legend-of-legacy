#!/usr/bin/env dotnet-script

#r "nuget: Yarhl, 4.0.0-preview.159"
#r "nuget: Yarhl.Media, 4.0.0-preview.159"
#r "nuget: YamlDotNet, 12.0.0"
#load "ExecutableConverters.csx"

using System.IO;
using Yarhl.FileSystem;
using Yarhl.FileFormat;
using Yarhl.Media.Text;
using Yarhl.IO;

if (Args.Count != 4) {
    Console.WriteLine("USAGE: CodeImporter <exHeaderPath> <DecompressedCode.bin> <poPath> <outputFolderPath>");
    return;
}

ImportProgramText(Args[0], Args[1], Args[2], Args[3]);

void ImportProgramText(string exHeaderPath, string codeBinPath, string poPath, string outputPath)
{
   // Create a copy to make modifications
    using var exHeader = DataStreamFactory.FromFile(exHeaderPath, FileOpenMode.Read);
    using var newExHeader = new DataStream();
    exHeader.WriteTo(newExHeader);

    // Decompress and compress .code
    // var decompressConverter = new ExternalProgramConverter {
    //    Program = $"GameData/blz",
    //    Arguments = "-d <inout>",
    // };

    var po = NodeFactory.FromFile(poPath, FileOpenMode.Read)
        .TransformWith<Binary2Po>()
        .GetFormatAs<Po>();

    NodeFactory.FromFile(codeBinPath, FileOpenMode.Read)
        // .TransformWith(decompressConverter)
        .TransformWith<Code3dsPoImporter, (Po, DataStream)>((po, newExHeader))
        .Stream.WriteTo($"{outputPath}/code.bin");

    // Citra doesn't support compressed code.bin, so we leave it decompressed
    // and we update the flag from the extended header
    newExHeader.Position = 0x0D;
    byte flags = (byte)newExHeader.ReadByte();
    flags &= 0xFE; // bit0: 1 code.bin is compressed, 0 decompressed
    newExHeader.Position = 0x0D;
    newExHeader.WriteByte(flags);
    newExHeader.WriteTo($"{outputPath}/exHeader");
}
