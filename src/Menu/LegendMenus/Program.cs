﻿using LegendMenus.Formats.Data;
using LegendMenus.Formats.Texts;
using System.CommandLine;
using Yarhl.FileSystem;
using Yarhl.IO;
using Yarhl.Media.Text;

var root = new RootCommand("Export and import the menu information of Legend of Legacy")
{
    CreateExportArguments(),
    CreateImportArguments(),
};
return await root.InvokeAsync(args).ConfigureAwait(false);

Command CreateExportArguments()
{
    var menuDatFile = new Option<FileInfo>("--menudat", "File containing the menu info")
        .ExistingOnly();
    menuDatFile.IsRequired = true;

    var menuTextFile = new Option<FileInfo>("--menutext", "File containing the menu text")
        .ExistingOnly();
    menuTextFile.IsRequired = true;

    var outputDir = new Option<DirectoryInfo>(
        new[] { "-o", "--output" },
        "Output directory to write the files");
    outputDir.IsRequired = true;

    var export = new Command("export", "Export the game files into editable formats")
    {
        menuDatFile,
        menuTextFile,
        outputDir,
    };
    export.SetHandler(Export, menuDatFile, menuTextFile, outputDir);

    return export;
}

Command CreateImportArguments()
{
    var menuDatFile = new Option<FileInfo>("--menuInfo", "YAML menu info file")
    .ExistingOnly();
    menuDatFile.IsRequired = true;

    var menuTextFile = new Option<FileInfo>("--menuPo", "PO menu file")
        .ExistingOnly();
    menuTextFile.IsRequired = true;

    var outputDir = new Option<DirectoryInfo>(
        new[] { "-o", "--output" },
        "Output directory to write the new binary files");
    outputDir.IsRequired = true;

    var import = new Command("import", "Import the editable files into game files")
    {
        menuDatFile,
        menuTextFile,
        outputDir,
    };
    import.SetHandler(Import, menuDatFile, menuTextFile, outputDir);

    return import;
}

void Export(FileInfo datFile, FileInfo textFile, DirectoryInfo outputDir)
{
    Console.WriteLine($"Exporting files into {outputDir.FullName}");

    using var datNode = NodeFactory.FromFile(datFile.FullName, FileOpenMode.Read);
    datNode.TransformWith<Binary2GameMenus>()
        .TransformWith<GameMenus2Yaml>()
        .Stream!.WriteTo(Path.Combine(outputDir.FullName, "menu.yml"));

    using var textNode = NodeFactory.FromFile(textFile.FullName, FileOpenMode.Read);
    textNode.TransformWith<Binary2MenuTexts>()
        .TransformWith<MenuTexts2Po>()
        .TransformWith<Po2Binary>()
        .Stream!.WriteTo(Path.Combine(outputDir.FullName, "menu.po"));

    Console.WriteLine("Done!");
}

void Import(FileInfo datFile, FileInfo textFile, DirectoryInfo outputDir)
{
    Console.WriteLine($"Importing files into {outputDir.FullName}");

    using var textNode = NodeFactory.FromFile(textFile.FullName, FileOpenMode.Read);
    textNode.TransformWith<Binary2Po>()
        .TransformWith<Po2MenuTexts>();

    using var datNode = NodeFactory.FromFile(datFile.FullName, FileOpenMode.Read);
    datNode.TransformWith<Yaml2GameMenus>()
        .TransformWith<GameMenus2Binary, MenuTexts>(textNode.GetFormatAs<MenuTexts>()!)
        .Stream!.WriteTo(Path.Combine(outputDir.FullName, "menu.dat"));

    string textOutputPath = Path.Combine(outputDir.FullName, "menu.text.txt");
    File.Delete(textOutputPath);
    textNode.TransformWith<MenuTexts2Binary>()
        .Stream!.WriteTo(textOutputPath);

    Console.WriteLine("Done!");
}
