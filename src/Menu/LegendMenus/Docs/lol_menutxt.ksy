meta:
  id: lol_menutxt
  application: "Legend of Legacy 3DS"
  file-extension: text
  license: MIT
  endian: le
doc: "menu.text with the list of texts for the menu"
seq:
  - id: texts
    type: strz
    encoding: utf8
    repeat: eos
