﻿# LegendMenu

## Format

### `menu.dat`

It contains a list of elements to show in different menus. The file contains the
elements in a _plain list_, the table at the beginning specifies how to group
these elements for each menu.

| Offset | Format        | Description                                          |
| ------ | ------------- | ---------------------------------------------------- |
| 0x00   | ushort        | Number of menus                                      |
| 0x02   | ushort[]      | Menu element start indexes (except for first menu 0) |
| ...    | MenuElement[] | Elements for each menu                               |

There is an _index_ per menu, except for the first menu which start index is 0.
The index specific the first element of that menu. We can get the last element
of that menu by reading the next _index_ or reading until the end of the file
for the last menu.

#### Menu elements

| Offset | Format | Description                  |
| ------ | ------ | ---------------------------- |
| 0x00   | ushort | Type                         |
| 0x02   | ushort | Parameter                    |
| 0x04   | uint   | ID for the variable text     |
| 0x08   | short  | Position X                   |
| 0x0A   | short  | Position Y                   |
| 0x0C   | ushort | Maximum bytes for the text?? |
| 0x0E   | ushort | Flags                        |

The types can be:

- 0: none
- 1: image
- 2: unknown
- 3: text
- 4: text but using sprites for the characters (typically numbers)
- 5: unknown

The parameter depends on the type of the element:

- 0: `0x00`
- 1: image index
- 2: unknown
- 3: font family: 1 normal, 2 cursive
- 4: unknown
- 5: unknown

The flags depends on the type as well. For the type _text_ (3), the meaning is
per bits:

- Bits 0-2: text color: 0 white, 2 black.
- Bits 3-15: text offset in `menu.text.txt`

### `menu.text`

List of strings encoded with UTF-8 and null-terminated.
