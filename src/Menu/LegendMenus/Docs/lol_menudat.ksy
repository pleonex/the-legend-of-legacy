meta:
  id: lol_menudat
  application: "Legend of Legacy 3DS"
  file-extension: dat
  license: MIT
  endian: le
doc: "menu.dat with unknown information so far"
seq:
  - id: num_groups
    type: u2
  - id: indexes
    type: u2
    repeat: expr
    repeat-expr: num_groups - 1 # it doesn't contain index[0] = 0
  - id: groups
    type: group(_index)
    repeat: expr
    repeat-expr: num_groups
instances:
  last_index:
    value: (_root._io.size - (num_groups * 2)) / 16
types:
  group:
    params:
      - id: index
        type: s4
    instances:
      start_index:
        value: index == 0 ? 0 : _parent.indexes[index - 1]
      end_index:
        value: index == _parent.num_groups - 1 ? _parent.last_index : _parent.indexes[index]
      num_entries:
        value: end_index - start_index
    seq:
      - id: entries
        size: 16
        type: entry
        repeat: expr
        repeat-expr: num_entries
  entry:
    seq:
      - id: type
        type: u2
        enum: entry_type
      - id: type_param # text: font, image: file
        type: u2
      - id: variable_id
        type: u4
      - id: pos_x
        type: s2
      - id: pos_y
        type: s2
      - id: text_max_size
        type: u2
      - id: text_color # 0: white, 2: black
        type: b3le
      - id: text_offset
        type: b13le
enums:
  entry_type:
    0 : none
    1: image
    3: text
    4: image_text