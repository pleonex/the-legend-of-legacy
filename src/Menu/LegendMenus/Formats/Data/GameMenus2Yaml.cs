﻿using YamlDotNet.Serialization;
using Yarhl.FileFormat;
using Yarhl.IO;

namespace LegendMenus.Formats.Data;

public class GameMenus2Yaml : IConverter<GameMenus, BinaryFormat>
{
    public BinaryFormat Convert(GameMenus source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var yamlSerializer = new SerializerBuilder()
            .Build();

        var menusYaml = yamlSerializer.Serialize(source);

        var binary = new BinaryFormat();
        var writer = new TextDataWriter(binary.Stream);
        writer.Write(menusYaml);

        return binary;
    }
}
