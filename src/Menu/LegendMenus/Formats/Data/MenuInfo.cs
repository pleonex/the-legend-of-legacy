﻿using System.Collections.ObjectModel;

namespace LegendMenus.Formats.Data;

public class MenuInfo
{
    public int Id { get; set; }

    public Collection<MenuElement> Elements { get; init; } = new Collection<MenuElement>();
}
