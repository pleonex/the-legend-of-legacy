﻿using LegendMenus.Formats.Texts;
using System.Text;
using Yarhl.FileFormat;
using Yarhl.IO;

namespace LegendMenus.Formats.Data;

public class GameMenus2Binary :
    IInitializer<MenuTexts>,
    IConverter<GameMenus, BinaryFormat>
{
    private readonly Dictionary<int, TextInfo> textOffsets = new();
    private MenuTexts? texts;

    public void Initialize(MenuTexts parameters)
    {
        texts = parameters;
    }

    public BinaryFormat Convert(GameMenus source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var binary = new BinaryFormat();
        var writer = new DataWriter(binary.Stream);

        writer.Write((ushort)source.Menus.Count);
        WriteIndexes(writer, source);

        RecalculateOffsets();
        foreach (var element in source.Menus.SelectMany(m => m.Elements))
        {
            WriteMenuElement(writer, element);
        }

        return binary;
    }

    private static void WriteIndexes(DataWriter writer, GameMenus menus)
    {
        // skip the first one
        int currentElements = menus.Menus.FirstOrDefault()?.Elements.Count ?? 0;
        for (int i = 1; i < menus.Menus.Count; i++)
        {
            writer.Write((ushort)currentElements);
            currentElements += menus.Menus[i].Elements.Count;
        }
    }

    private void WriteMenuElement(DataWriter writer, MenuElement element)
    {
        writer.Write((ushort)element.Kind);
        writer.Write((ushort)element.Parameter);
        writer.Write((uint)element.VariableId);
        writer.Write((short)element.PositionX);
        writer.Write((short)element.PositionY);

        if (element.Kind == MenuElementKind.Text && textOffsets.TryGetValue(element.TextOffset, out TextInfo textInfo))
        {
            element.TextOffset = textInfo.TextOffset;

            // no idea what's the logic here as for texts of 0x25 bytes it loads 0x38... so we only update if it's shorter
            if (element.TextMaxSize < textInfo.TextSize)
            {
                int diff = textInfo.TextSize - element.TextMaxSize;
                element.TextMaxSize += diff;
            }
        }

        writer.Write((ushort)element.TextMaxSize);
        ushort textFlags = (ushort)((element.TextOffset << 3) | (ushort)element.TextColor);
        writer.Write(textFlags);
    }

    private void RecalculateOffsets()
    {
        if (texts is null)
        {
            return;
        }

        textOffsets.Clear();
        var encoding = Encoding.UTF8;

        int currentOffset = 0;
        foreach (var entry in texts.Entries)
        {
            int textLength = encoding.GetByteCount(entry.Text) + 1; // null terminator
            var info = new TextInfo { TextOffset = currentOffset, TextSize = textLength };
            textOffsets.Add(entry.Id, info);

            currentOffset += textLength;
        }
    }

    private readonly struct TextInfo
    {
        public int TextSize { get; init; }

        public int TextOffset { get; init; }
    }
}
