﻿using Yarhl.FileFormat;
using Yarhl.IO;

namespace LegendMenus.Formats.Data;

public class Binary2GameMenus : IConverter<IBinary, GameMenus>
{
    private const int MenuInfoSize = 0x10;

    public GameMenus Convert(IBinary source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var menus = new GameMenus();

        source.Stream.Position = 0;
        var reader = new DataReader(source.Stream);

        ushort numMenus = reader.ReadUInt16();
        int menuElementsOffset = numMenus * sizeof(ushort);
        int totalElements = (int)(source.Stream.Length - menuElementsOffset) / MenuInfoSize;

        for (int i = 0; i < numMenus; i++)
        {
            // the start index for the first menu (0) is not saved in the file.
            int startElementIdx = i > 0 ? reader.ReadUInt16() : 0;
            reader.Stream.PushCurrentPosition();

            int nextElementIdx = i + 1 < numMenus ? reader.ReadUInt16() : totalElements;
            int numElements = nextElementIdx - startElementIdx;

            int startMenuOffset = menuElementsOffset + startElementIdx * MenuInfoSize;
            reader.Stream.Seek(startMenuOffset);

            var menu = ReadMenu(reader, numElements);
            menu.Id = i;
            menus.Menus.Add(menu);

            reader.Stream.PopPosition();
        }

        return menus;
    }

    private MenuInfo ReadMenu(DataReader reader, int numElements)
    {
        var menu = new MenuInfo();
        for (int i = 0; i < numElements; i++)
        {
            var element = ReadMenuElement(reader);
            menu.Elements.Add(element);
        }

        return menu;
    }

    private MenuElement ReadMenuElement(DataReader reader)
    {
        var element = new MenuElement();
        element.Kind = (MenuElementKind)reader.ReadUInt16();
        element.Parameter = reader.ReadUInt16();
        element.VariableId = reader.ReadUInt32();
        element.PositionX = reader.ReadInt16();
        element.PositionY = reader.ReadInt16();
        element.TextMaxSize = reader.ReadUInt16();

        ushort textInfo = reader.ReadUInt16();
        element.TextColor = (TextColors)(textInfo & 0x07);
        element.TextOffset = textInfo >> 3;

        return element;
    }
}
