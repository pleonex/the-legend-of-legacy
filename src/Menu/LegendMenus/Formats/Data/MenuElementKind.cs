﻿namespace LegendMenus.Formats.Data;

public enum MenuElementKind
{
    None = 0,
    Image = 1,
    Text = 3,
    TextImage = 4,
}
