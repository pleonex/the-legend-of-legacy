﻿namespace LegendMenus.Formats.Data;

public class MenuElement
{
    public MenuElementKind Kind { get; set; }

    public int Parameter { get; set; }

    public uint VariableId { get; set; }

    public int PositionX { get; set; }

    public int PositionY { get; set; }

    public int TextMaxSize { get; set; }

    public TextColors TextColor { get; set; }

    public int TextOffset { get; set; }
}
