﻿using System.Collections.ObjectModel;
using Yarhl.FileFormat;

namespace LegendMenus.Formats.Data;

public class GameMenus : IFormat
{
    public Collection<MenuInfo> Menus { get; init; } = new Collection<MenuInfo>();
}
