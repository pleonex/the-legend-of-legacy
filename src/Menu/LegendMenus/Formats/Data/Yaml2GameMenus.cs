﻿using YamlDotNet.Serialization;
using Yarhl.FileFormat;
using Yarhl.IO;

namespace LegendMenus.Formats.Data;

public class Yaml2GameMenus : IConverter<IBinary, GameMenus>
{
    public GameMenus Convert(IBinary source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var deserializer = new DeserializerBuilder()
            .Build();

        var yaml = new TextDataReader(source.Stream).ReadToEnd();
        var menus = deserializer.Deserialize<GameMenus>(yaml);

        return menus;
    }
}
