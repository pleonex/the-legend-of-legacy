﻿using System.Text;
using Yarhl.FileFormat;
using Yarhl.IO;

namespace LegendMenus.Formats.Texts;

public class MenuTexts2Binary : IConverter<MenuTexts, BinaryFormat>
{
    public BinaryFormat Convert(MenuTexts source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var binary = new BinaryFormat();
        var writer = new DataWriter(binary.Stream);
        writer.DefaultEncoding = Encoding.UTF8;

        foreach (var entry in source.Entries)
        {
            writer.Write(entry.Text, nullTerminator: true);
        }

        return binary;
    }
}
