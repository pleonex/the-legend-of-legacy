﻿using Yarhl.FileFormat;
using Yarhl.Media.Text;

namespace LegendMenus.Formats.Texts;

public class Po2MenuTexts : IConverter<Po, MenuTexts>
{
    public MenuTexts Convert(Po source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var texts = new MenuTexts();
        foreach (var entry in source.Entries)
        {
            string text = entry.Text;
            if (text == "<empty/>")
            {
                text = string.Empty;
            }

            var textEntry = new TextEntry
            {
                Text = text,
                Id = int.Parse(entry.Context),
            };
            texts.Entries.Add(textEntry);
        }

        return texts;
    }
}
