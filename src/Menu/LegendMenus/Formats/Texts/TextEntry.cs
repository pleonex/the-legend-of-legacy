﻿namespace LegendMenus.Formats.Texts;

public class TextEntry
{
    public string Text { get; set; } = string.Empty;

    public int Id { get; set; }
}
