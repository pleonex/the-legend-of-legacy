﻿using Yarhl.FileFormat;
using Yarhl.Media.Text;

namespace LegendMenus.Formats.Texts;

public class MenuTexts2Po : IConverter<MenuTexts, Po>
{
    public Po Convert(MenuTexts source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var header = new PoHeader("legend-legacy-menus", "TraduSquare", "en-US");
        var po = new Po(header);

        foreach (var entry in source.Entries)
        {
            var poEntry = new PoEntry
            {
                Context = $"{entry.Id}",
                Original = (entry.Text.Length == 0) ? "<empty/>" : entry.Text,
            };
            po.Add(poEntry);
        }

        return po;
    }
}
