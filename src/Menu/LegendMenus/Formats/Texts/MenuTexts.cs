﻿using System.Collections.ObjectModel;
using Yarhl.FileFormat;

namespace LegendMenus.Formats.Texts;

public class MenuTexts : IFormat
{
    public Collection<TextEntry> Entries { get; } = new Collection<TextEntry>();
}
