﻿using System.Text;
using Yarhl.FileFormat;
using Yarhl.IO;

namespace LegendMenus.Formats.Texts;

public class Binary2MenuTexts : IConverter<IBinary, MenuTexts>
{
    public MenuTexts Convert(IBinary source)
    {
        ArgumentNullException.ThrowIfNull(source);

        var texts = new MenuTexts();

        var reader = new DataReader(source.Stream);
        reader.DefaultEncoding = Encoding.UTF8;

        while (!reader.Stream.EndOfStream)
        {
            var entry = new TextEntry
            {
                Id = (int)source.Stream.Position,
                Text = reader.ReadString(),
            };
            texts.Entries.Add(entry);
        }

        return texts;
    }
}
