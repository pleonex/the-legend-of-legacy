msgid ""
msgstr ""
"Project-Id-Version: Legend of Legacy\n"
"Report-Msgid-Bugs-To: transin\n"
"POT-Creation-Date: 2/09/21\n"
"PO-Revision-Date: 2022-01-13 13:40+0000\n"
"Last-Translator: Jorge Guerra <joguersan@gmail.com>\n"
"Language-Team: Spanish <https://weblate.tradusquare.es/projects/"
"the-legend-of-legacy/d07_shsky/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9\n"

#. Dialog
msgctxt "0"
msgid ""
"Does anyone else see\n"
"a pair of eyes...!? *gulp*"
msgstr ""
"¡¿Vosotros también\n"
"habéis visto unos ojos?!\n"
"Glup..."

#. Empty
msgctxt "1"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "2"
msgid ""
"Something is hiding\n"
"in there for us..."
msgstr ""
"Algo acecha entre\n"
"las sombras..."

#. Empty
msgctxt "3"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "4"
msgid "Something's lying in wait."
msgstr "Algo está al acecho."

#. Empty
msgctxt "5"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "6"
msgid ""
"Oh no! We're about to\n"
"have company, everyone!"
msgstr ""
"¡Vaya, parece que tenemos\n"
"compañía, equipo!"

#. Empty
msgctxt "7"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "8"
msgid ""
"It is an ambush!\n"
"Prepare for battle!"
msgstr ""
"¡Es una emboscada!\n"
"¡Preparaos para luchar!"

#. Empty
msgctxt "9"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "10"
msgid ""
"Hm... maybe he just\n"
"wants to be friends?"
msgstr ""
"Mmm... ¿Y si solo quiere\n"
"ser nuestro amigo?"

#. Empty
msgctxt "11"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "12"
msgid ""
"Piles of debris do not\n"
"normally possess eyes...!\n"
"GIRD YOUR LOINS, MEN!"
msgstr ""
"Lo normal es que una montaña\n"
"de escombros no tenga ojos, ¿no?\n"
"¡Armaos de valor, muchachos!"

#. Empty
msgctxt "13"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "14"
msgid ""
"You hear something!?\n"
"...Must be my imagination."
msgstr ""
"¡¿Habéis oído eso?!\n"
"Serán imaginaciones mías."

#. Dialog
msgctxt "15"
msgid ""
"Say, you think that turtle\n"
"hid any treasure 'round\n"
"here?"
msgstr ""
"¿Creéis que esa tortuga\n"
"tendría algún tesoro\n"
"escondido por aquí?"

#. Empty
msgctxt "16"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "17"
msgid ""
"Did I just hear water...?\n"
"Must be my imagination."
msgstr ""
"¿Eso que suena es agua?\n"
"Serán imaginaciones mías."

#. Empty
msgctxt "18"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "19"
msgid ""
"This place feels strange...\n"
"Must be my imagination."
msgstr ""
"Algo me huele a chamusquina...\n"
"Serán imaginaciones mías."

#. Empty
msgctxt "20"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "21"
msgid ""
"Hm? This place feels strange...\n"
"Maybe it's my imagination."
msgstr ""
"¿Eh? Algo no me cuadra...\n"
"Serán imaginaciones mías."

#. Empty
msgctxt "22"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "23"
msgid ""
"Hm? This place feels strange...\n"
"Must be my imagination."
msgstr ""
"¿Eh? Qué raro, este lugar...\n"
"No, serán imaginaciones mías."

#. Empty
msgctxt "24"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "25"
msgid ""
"Hm? This place feels strange...\n"
"Maybe it's my imagination."
msgstr ""
"¿Eh? Algo no me cuadra...\n"
"Serán imaginaciones mías."

#. Empty
msgctxt "26"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "27"
msgid ""
"I have a strange feeling about\n"
"this... My frog sense is tingling..."
msgstr ""
"Tengo un mal presentimiento...\n"
"Mi sentido batracio se ha activado..."

#. Dialog
msgctxt "28"
msgid ""
"Er, maybe not! Perhaps it was\n"
"just my tummy."
msgstr ""
"¡O puede que no!\n"
"Creo que solo es mi tripa."

#. Empty
msgctxt "29"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "30"
msgid ""
"Huh!? The <yellow>Shadow</color>\n"
"<yellow>Stone</color> is pulsing...!"
msgstr ""
"¡¿Eh?! ¡La <yellow>piedra sombría</color>\n"
"está palpitando!"

#. Empty
msgctxt "31"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "32"
msgid ""
"The <yellow>Shadow Stone</color>\n"
"is glowing..."
msgstr ""
"La <yellow>piedra sombría</color>\n"
"está brillando..."

#. Empty
msgctxt "33"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "34"
msgid ""
"Hm... The <yellow>Shadow</color>\n"
"<yellow>Stone</color> is pulsing."
msgstr ""
"Mmm... La <yellow>piedra sombría</color>\n"
"está brillando."

#. Empty
msgctxt "35"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "36"
msgid ""
"Huh!? The <yellow>Shadow</color>\n"
"<yellow>Stone</color> is pulsing...!"
msgstr ""
"¿¡Eh!? ¡La <yellow>piedra sombría</color>\n"
"está brillando!"

#. Empty
msgctxt "37"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "38"
msgid ""
"The <yellow>Shadow Stone</color>\n"
"is starting to glow..."
msgstr ""
"La <yellow>piedra sombría</color>\n"
"está empezando a brillar..."

#. Empty
msgctxt "39"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "40"
msgid ""
"Huh!? The <yellow>Shadow</color>\n"
"<yellow>Stone</color> is glowing...!"
msgstr ""
"¡¿Eh?! ¡La <yellow>piedra sombría</color>\n"
"está brillando!"

#. Empty
msgctxt "41"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "42"
msgid ""
"Hoi! The <yellow>Shadow Stone</color>\n"
"has started pulsing...!\n"
"What does this portend?"
msgstr ""
"¡¿Eh?! ¡La <yellow>piedra sombría</color>\n"
"está brillando!\n"
"¿Qué estará presagiando?"

#. Empty
msgctxt "43"
msgid "<!empty>"
msgstr "<!empty>"

#. Other
msgctxt "44"
msgid "Jump over?"
msgstr "¿Saltar?"

#. Empty
msgctxt "45"
msgid "<!empty>"
msgstr "<!empty>"
